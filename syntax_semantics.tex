%%
%%% Section
%%

\section{Process syntax and semantics}

The syntax for \emph{events} and \emph{processes} is displayed in \figurename~\ref{fig:process syntax}. For sake of simplicity, we only present a simplified version of syntax and semantics as the additional elements (e.g. tables and phases) does not impact the results presented in this paper.

\begin{figure}[ht]
\begin{center}
\begin{minipage}{\columnwidth}
  \begin{tabbing}
    $ev$ ::= \= \hspace{7cm} \=    eve\=nts\\
    \> $e(M_1,\ldots, M_k)$ \>\> ($e\in \Fe$)\\
    \\
    $P,Q$ ::=\>\> processes\\
    \>$0$\>\> nil\\
    \>$\out(N,M);P$\>\>output\\
    \>$\inn(N,x);P$\>\>input\\
    \>$P \mid Q$\>\>parallel composition\\
    \>$!P$\>\>replication\\
    \>$\new a;P$\>\>restriction\\
    \>$\llet x=D \inelse P \eelse Q$\>\>assignment\\
    \>$\event(ev);P$\>\>event
  \end{tabbing}
  \end{minipage}
\end{center}
\caption{Process syntax.}
\label{fig:process syntax}
\end{figure}

A \emph{configuration} $\E, \Pro, \Att$ is given by a multiset $\Pro$ of processes, representing the current state of the process, a set of names $\E$ representing the free names of $\Pro$ and the names created by the adversary, and a set $\Att$ of terms known by the adversary. The semantics of processes is defined through a reduction relation $\ostep{\ell}$ between configuration, defined in \cref{fig:semantics}, where $\ell$ is either the empty label or a label of the form $\lmess(M,N)$ or $\levent(ev)$ with $M,M$ being terms and $ev$ being an event. These labels will be used to define the satisfiability of predicates and queries. 

An initial configuration is a closed configuration of the form $\E,\multiset{P},\Att$ where $\Att$ contains only names and $\E$ is the union of $\Att$ with the free names of $P$.

\begin{figure}[ht]
\[
\begin{array}{@{}lr}
	\E,\Pro \cup \multiset{0},\Att \ostep{} \E, \Pro,\Att &(\RNil)\\[1mm]
  %
	\E,\Pro \cup \multiset{P \mid Q},\Att \ostep{} \E,\Pro \cup \multiset{P,Q},\Att &(\RPar)\\[1mm]
  %
	\E, \Pro \cup \multiset{!P},\Att \ostep{} \E, \Pro \cup \multiset{P,!P},\Att &(\RRepl)\\[1mm]
  %
	\E, \Pro \cup \multiset{\new a;P},\Att \ostep{} \E \cup \set{a'}, \Pro \cup \multiset{P\{^{a'}/_a\}},\Att \qquad \hfill\text{if $a' \not\in \E$}&(\RNew)\\[1mm]
  %
	\E,\Pro \cup \multiset{\out(N,M);P, \inn(N,x);Q},\Att \ostep{\lmess(N,M)} \E, \Pro \cup \multiset{P, Q\{^M/_x\}},\Att&(\RIO)\\[1mm]
  %
	\E, \Pro, \Att \ostep{\lmess(N,M)} \nphase, \E, \Pro,\Att \qquad \hfill\text{if $N,M \in \Att$}&(\RMess)\\[1mm]
  %
	\E,\Pro \cup \multiset{\llet x=D \inelse P \eelse Q}, \Att \ostep{} \E,\Pro\cup\multiset{P\{^M/_x\}}, \Att\quad\hfill\text{if $D \Eval{\Sigma} M$}&(\RLetin)\\[1mm]
  %
	\E,\Pro \cup \multiset{\llet x=D \inelse P \eelse Q}, \Att \ostep{} \E,\Pro\cup\multiset{Q}, \Att\quad\hfill\text{if $D \Eval{\Sigma} \fail$}&(\RLetelse)\\[1mm]
  %
	\E,\Pro \cup \multiset{\out(N,M);P},\Att \ostep{\lmess(N,M)} \E, \Pro \cup \multiset{P},\Att \cup \set{M}\quad\hfill\text{if $N \in \Att$} &(\ROut)\\[1mm]
  %
	\E,\Pro \cup \multiset{\inn(N,x);Q},\Att \ostep{\lmess(N,M)} \E, \Pro \cup \multiset{Q\{^M/_x\}},\Att \quad\hfill\text{if $N,M \in \Att$}&(\RIn)\\[1mm]
	%
	\E,\Pro,\Att \ostep{} \E, \Pro,\Att \cup \set{M}&(\RApp)\\
	\hfill\text{if $M_1,\ldots, M_n \in \Att$, $f/n \in \Fc \cup \Fd$ and $f(M_1,\ldots,M_n) \Eval{\Sigma} M$}&\\[1mm]
	%
	\E,\Pro,\Att \ostep{} \E \cup \set{a'},\Pro,\Att \cup \set{a'}\quad\hfill\text{if $a' \not\in \E$}&(\RFree)\\[1mm]
	%
	\E,\Pro \cup \multiset{\event(ev); P},\Att \ostep{\levent(ev)} \E,  \Pro \cup \multiset{P},\Att &(\REvent)
\end{array}
\]
\caption{Transitions between configurations.}
\label{fig:semantics}
\end{figure}

The semantics is directly taken from~\cite{BCC-snp22} and follows the usual intuitive. The reduction rule \RNil removes the process~$0$. The rule \RPar corresponds to the parallel composition. The rule \RRepl duplicates the process $P$. The rule \RNew generates a fresh private name, i.e. not in $\E$. Rules \RMess, \RIn and \ROut models the output and input communication, either internal or external. 
All these rules are labeled by $\lmess(N,M)$ indicating that a message $M$ as been sent over the channel $N$.
The rule \REvent executes the event $M$. Finally, the rules \RLetin and \RLetelse define the semantics of the evaluation of an expression $D$, either resulting to a message or else a failure. Note that the evaluation is with respect to the main signature $\Sigma$.

%%
%%% Subsection
%%

\subsection{Security queries}

Similarly to syntax and semantics, we only focus on a simplified class of queries than the queries that ProVerif can handle. In particular, we will only look at secredy and correspondence queries and let the reader adapt our work to injective, temporal and observational queries.

To express secrecy and correspondence properties, we consider \emph{facts} given by the following grammar:
% \begin{center}
% \begin{minipage}{\columnwidth}
% \begin{tabbing}
% 	$F$ ::=\=\hspace{4.3cm}\=fact\=\\
% 	\> $\pevent(e(M_1,\ldots,M_n))$ \>\> an event\\
% 	\> $\patt{\nphase}(M)$ \>\> the attacker knows $M$ at phase $\nphase$\index{attacker fact@$\attacker{\nphase}(M)$, attacker fact}\\
% 	\> $\pmess{\nphase}(M,N)$ \>\> the message $N$ was sent on the channel $M$ at phase $\nphase$\index{message fact@$\mess{\nphase}(M,N)$, message fact}\\

% 	\\
% 	$\phi$\index{formula@$\phi$, formula} ::=\>\>generic formula\>\\
% 	\> $M = N$ \>\> an equality\\
% 	\> $M \neq N$ \>\> a disequality\\
% 	\\
% 	$\alpha$\index{atomic@$\alpha$, atomic formula} ::=\>\>atomic formula\>\\
%   	\> $\tF$ \>\> a temporal fact\\
%   	\> $\phi$ \>\> a generic predicate\\
%   	\\
%   	$\querycl,\querycl'$ ::=\>\>query conclusion\>\\
%   	\> $\top$ \>\> true\\
%   	\> $\bot$ \>\> false\\
%   	\> $\querycl \wedge \querycl'$\>\> a conjunction\\
%   	\> $\querycl \vee \querycl'$\>\> a disjunction\\
%   	\> $\alpha$ \>\> an atomic formula\\
%   	\\
%   	$\query$ \index{correspondence@$\bigwedge_{i=1}^n \tF_i \Rightarrow \querycl$, correspondence query}::=\>\> correspondence query \>\\
%   	\> $\tF_1 \wedge \ldots \wedge \tF_n \Rightarrow \querycl$\>\>\\
% \end{tabbing}
% \end{minipage}
% \end{center}

%%
%%% Subsection
%%

\subsection{Limiting to search space}



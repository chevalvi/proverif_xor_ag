%%
%%% Subsection
%%

\subsection{Extended signature}

In this section, we show how we transform a signature and in particular its equational theory so that it can be efficiently handled by ProVerif. As what is done~\cite{CB-post13}, we transform the sequence of rewrite rules describing the semantics of destructors into a set of rewrite rules by extending the notion of rewrite rules:
\[
g(U_1,\ldots,U_n) \rightarrow U \with \phi
\]
where $\phi$ is a conjunction of disequations of the form $\forall \tilde{z}. M \neq N$. Given an equational theory $E$ and a substitution $\sigma$, we denote by $\sigma \models_E \phi$ the satisfaction of $\phi$ where the disequations are considered modulo $E$. 
Additionnaly, for a constructor function symbol $f$, we consider the following rewrite rules denoted $\Rdef(f)$:
\[
\begin{array}{c}
f(x_1,\ldots,x_n) \rightarrow f(x_1,\ldots,x_n)\\
f(\fail,u_2,\ldots,u_n) \rightarrow \fail\\
\ldots\\
f(x_1,\ldots,x_i,\fail,u_{i+2},\ldots,u_n) \rightarrow \fail\\
\ldots\\
f(x_1,\ldots,x_{n-1},\fail) \rightarrow \fail\\
\end{array}
\]
Let us denote by $\defrw{\R}(g)$ the set of rewrite rules $f(U_1,\ldots, U_n) \rightarrow U \with \phi$ in $\R$ such that $f = g$. 
We define a \emph{set-based signature} $\SSigma$ as a pair $(\R,E)$ where $\R$ is a set of extended rewrite rules and $E$ an equational theory such that for all $f \in \Fc$, $\defrw{\R}(f) = \Rdef(f)$.


We augment expressions to include may-fail variables and so may-fail terms. 
With $\Sigma' = (\R,E)$ and $\R$ an set of extended rewrite rule, the evaluation $\Eval{\Sigma'}$ is naturally updated as: 
\begin{itemize}
\item $U \Eval{\Sigma'} U$
\item $h(D_1,\ldots,D_n) \Eval{\Sigma'} U\sigma$ if $h(U_1,\ldots,U_n) \rightarrow U \with \phi$ in $\R$, for all $i=1\ldots n$, $D_i \Eval{\Sigma'} V_i$ and $V_i =_E U_i\sigma$ and $\sigma \models_E \phi$
\end{itemize}


In the spirit of~\cite{BlanchetAbadiFournetJLAP07}, we transforms a signature $\Sigma$ into a signture $\Sigma'$ where the associated equational theory is easier to handle. In~\cite{BlanchetAbadiFournetJLAP07}, this new equational theory is in fact the syntactic equality, i.e. the empty equational theory. This corresponds to expressing equational theory with a rewrite system. In this paper, we allow the associated equational theory to be more elaborated, e.g. XOR, DH, AC, \ldots. Additionally, for each constructor function symbol $f$, we associated a \emph{to be evaluated constructor function symbol} denoted $\symbeval{f}$ that will correspond to a symbol that needs to be evaluated.

%%
%%% Definition
%%

\begin{definition}\label{def_ext_sig}
We define an extended signature $\ESigma$ as the tuple $(\Rm,\Rn,\En,\Ea,E)$ where:
\begin{itemize}
  \item $\En$, $\Ea$ and $E$ are equational theory with $\En \subseteq \Ea \subseteq E$ and $\En$ is regular
  \item $\Rn$ is a $\En$-convergent rewrite system
  \item $\Rm$ is an extended rewrite system such that  
  \begin{enumerate}
  \item for all $f \in \Fc$, $\defrw{\Rm}(f) = \Rdef(f)$ and $\Rdef(\symbeval{f}) \subseteq \defrw{\Rm}(\symbeval{f})$
  \item for all $\symbeval{f}(U_1,\ldots, U_n) \rightarrow U \with \phi$ in $\Rm$, $\phi = \top$
  \end{enumerate}
  \item for all $t,t'$, $t \rwstep{\Rn}{\En} t'$ implies $t =_E t'$; and $t =_{\Ea} t'$ implies $t \norm{\Rn}{\En} =_{\En} t' \norm{\Rn}{\En}$
\end{itemize}
\end{definition}

Intuitively, $\Ea$ is the equational theory for which we have a
(partial) unification algorithm, e.g. XOR, DH, AC, A. The rewrite
system $\Rn$ and the equational theory $\En$ acts as a way to
normalise our terms. In particular, we will not aim to unify modulo
$\Rn,\En$ but only put terms in normal form which is simpler.

\todo[inline]{How to address ``partial'' unification? As in Tamarin we
  can ``drop'' (probably not the good word) predicates we are not able
  to handle\ldots or we can keep them in the bag, hoping to be able to
  handle them later in the process. This later approach will be of
  great help, e.g. to handle hash functions built with Merkle-Damgard
  rationale.}

%%
%%% Definition
%%

\begin{definition}[Evaluation on extended signature]
Given $\ESigma = (\Rm,\Rn,\En,\Ea,E)$ an extended signature, we define the evaluation of an expression $D$, denoted $D \Eval{\ESigma} U$ as follows:
\begin{itemize}
\item $U \Eval{\ESigma} U$
\item $f(D_1,\ldots,D_n) \Eval{\ESigma} U\sigma$ if for $i = 1 \ldots n$, $D_i\Eval{\ESigma} V_i$ and $f(U_1,\ldots, U_n) \rightarrow U \with \phi$ in $\Rm$, $\sigma \models_E \phi$ and for $i = 1\ldots n$, $D_i\Eval{\ESigma} V_i$, $V_i =_{\Ea} U_i\sigma$
\end{itemize}
\end{definition}
Notice that we use the equational theory $\Ea$ when matching the arguments of the rewrite rules but we still use the equational theory $E$ when considering the satisfaction of $\phi$.
  
Thanks to the rewrite system $\Rn$ and its associated equational theory $\En$, we can define the notion of normal with respect to an extended signature $\ESigma$.

%%
%%% Definition
%%

\begin{definition}
Let $\ESigma = (\Rm,\Rn,\En,\Ea,E)$ be an extended signature. Given a set of terms $\Mset$, we say that $\Mset$ is \emph{in normal form w.r.t. $\ESigma$}, denoted $\nf{\ESigma}{\Mset}$, when:
\begin{itemize}
\item for all $t \in \st{\Mset}$, $t \norm{\Rn}{\En} = t$
\item for all $t,t' \in \st{\Mset}$, $t =_E t'$ implies $t =_{\En} t'$
\end{itemize}
\end{definition}


%%
%%% Subsection
%%

\paragraph{Open evaluation}
To defined the how an extended signature models a standard signature, we first need to define evaluation on open terms and expressions, as a relation $D \OEval{\ESigma} (U,\sigma,\phi)$ where $\sigma$ are the instantiations of $D$ obtained by evalution and $\phi$ collects the side conditions of destructor applications.

\begin{definition}[Open evaluation]
Let $\ESigma = (\Rm,\Rn,\En,\Ea,E)$ be an extended signature. Let $D$ be an expression, we define the open evaluation of $D$, denoted $D \OEval{\ESigma} (U,\sigma,\phi)$, as follows:

\[
\begin{minipage}{\columnwidth}
\begin{tabbing}
$U \OEval{\ESigma} (U, \emptyset, \top)$ \\[1.5mm]
$f(D_1, \ldots, D_n) \OEval{\ESigma} (V\sigma_u, \sigma'\sigma_u,  \phi'\sigma_u  \wedge  \phi\sigma_u)$
\\
\quad if $(D_1, \ldots, D_n) \OEval{\ESigma} ((U_1, \ldots, U_n),\sigma', \phi')$, \\
\quad $f(V_1, \ldots, V_n) \rightarrow V \with \phi \in \R'$ and\\ 
\quad $\sigma_u \in \mguAlg{\Ea}{(U_1,\ldots,U_n),(V_1,\ldots,V_n)}$
\\[1.5mm]
$(D_1, \ldots, D_n) \OEval{\ESigma} ((U_1\sigma_n , \ldots, 
 U_{n-1}\sigma_n , U_n),  \sigma \sigma_n,  \phi\sigma_n  \wedge \phi_n)$\\
\quad if $(D_1, \ldots, D_{n-1}) \OEval{\ESigma}
((U_1, \ldots, \allowbreak U_{n-1}), \sigma, \phi)$ 
and $D_n\sigma \OEval{\ESigma} (U_n,\sigma_n, \phi_n)$
\end{tabbing}
\end{minipage}
\]
\end{definition}

Given an expression $D$, we denote by $\addeval{D}$ the expression obtained from $D$ by replacing all constructor function symbol $f$ by $\symbeval{f}$.
We can now defined when an extended signature $\ESigma$ models a signature $\Sigma$.

%%
%%% Definition
%%

\begin{definition}
Let $\SSigma = (\R,E)$ be a set-based signature. We say that an extended signature $\ESigma$ models $\SSigma$ if $\ESigma = (\Rm,\Rn,\En,\Ea,E)$ for some $\Rm,\Rn,\En,\Ea$ such that:
\begin{enumerate}[label=\textbf{S\arabic*}]
\item \label{S:subeq} If $\nf{\ESigma}{\Mset}$ then for all $M$, there exists $M'$ such that $M =_E M'$ and $\nf{\ESigma}{\Mset \cup \{M'\}}$.
  
\item \label{K:eqcomplete} If $\symbeval{f}(N_1, \ldots, N_n) \rightarrow N$ is in $\Rm$ then $f(N_1, \ldots, N_n) \eqE{E} N$.
    
\item \label{S:ind1} If $f(M_1, \ldots, M_n) =_E M$, $f \in \Fc$ and $\nf{\ESigma}{\{ M_1,\ldots,M_n, M \}}$ then there exist $\sigma$ and $\symbeval{f}(N_1,\ldots, N_n) \rightarrow N$ in $\Rm$ such that $M =_{\Ea} N\sigma$ and $M_i =_{\Ea} N_i\sigma$ for all $i\in \{ 1, \ldots, n \}$.

\item \label{S:destr} if $g \in \Fd$ then $\defrw{\Rm}(g)$ is the set of rules $g(U'_1,\ldots,U'_n) \rightarrow U' \with \phi\sigma \wedge \phi'$ such that $g(U_1,\ldots,U_n) \rightarrow U \with \phi$ is in $\defrw{\R}$ and $(\addeval{U_1},\ldots,\addeval{U_n},\addeval{U}) \OEval{\ESigma} ((U'_1,\ldots,U'_n,U'),\sigma,\phi')$

\item \label{S:norm} if $f(U_1, \ldots, U_n) \rightarrow U \with \phi$ in $\Rm$ then $\nf{\ESigma}{\{ U_1,\ldots,U_n,U\}}$.
\end{enumerate}
\end{definition}

%%
%%% Subsection
%%

\subsection{Examples}

Add somewhere in the equational theory the basic equations, such as $\{dec(enc(m,k),k)=m, fst(x,y)=x, snd(x,y)=y\}$\ldots

\todo[inline]{Put some examples of signature and their modelisations}

We have to keep in mind that all the rewriting rules that will be in
$\Rm$ will lead to generation of Horn clauses in Proverif. In order to
limit the number of Horn clauses to process, we can try to ``remove''
some rules from $\Rm$ and put them in $\Ea$ or $\En$. Those put in
$\En$ will benefit from ``normalization'' (at unification
cost). Hence, what is not clear for the moment (and will be clarified
through experiments) is which strategy will be better, asking for more
unification vs. Horn clauses.

Before setting up examples, let us briefly recall the equational
theories and related rewriting rules (setup here with symbol '$*$'
and neutral element $1$, may be transposed to symbol '$+$' and neutral
element $0$) :

\hspace*{-3cm}
\begin{tabular}{|c|c|c|c|c|c|c|}
  \hline
  {\tiny notion} & acronym & equations & reg? & ori? & unif? & rew. rules \\
  \hline
  \hline
  {\tiny Associativity} & A & $ \{ x * ( y * z ) = ( x * y ) * z \}$ & yes & & & \\
  \hline
  {\tiny Commutativity} & C & $ \{ x *  y  =  y * x \}$ & yes & & & \\
  \hline
  & AC & A $\cup$ C & yes & & & $x * x * y \rightarrow y $ \\
  \hline
  {\tiny Unit} & U & $\{ x * 1 = x \}$ & yes & yes & & $ x * 1 \rightarrow x $ \\
  \hline
  {\tiny Nilpotency} & N & $\{ x * x = 1 \}$ & no & yes & & $ x * x \rightarrow 1 $ \\
  \hline
  {\tiny XOR} & ACUN & AC & & & & $x * x * y \rightarrow y $ \\
                 & & $\cup$ U & & & & $ x * 1 \rightarrow x $ \\
                 & & $\cup$ N & & & & $ x * x \rightarrow 1 $ \\
  \hline
  {\tiny Idempotency} & I & $\{ x * x = x \}$ & yes & yes & & $ x * x\rightarrow x $ \\
  \hline
  {\tiny Abelian Group} & AG & AC & no & yes & & $ x * x * y \rightarrow y $ \\
                 & & $\cup$ U & & & & $ x * 1 \rightarrow x $\\
                 & & $\cup$ $\{ x * x^{-1} = 1\}$ & & & & $ x * x^{-1} \rightarrow 1 $\\
  \hline
  {\tiny Diffie Hellman} & DH & AG & & & & $ x * x * y \rightarrow y $ \\
                 & & & & & & $ x * 1 \rightarrow x $\\
                 & & & & & & $ x * x^{-1} \rightarrow 1 $\\
                 & & $\cup$ $\{ exp(x,1) = x \}$  & & & & $ exp(x,1) \rightarrow x  $\\
                 & & $\cup$ $\{ exp(exp(x,y),z) = exp(x,y*z)\}$ & & & & $ exp(exp(x,y),z) \rightarrow exp(x,y*z) $\\
  \hline
{\tiny homomorphy} & h & $\{ h(x+y) = h(x) + h(y) \}$ & yes & & &
                                                                  $h(x)
                                                                  +
                                                                  h(y)
                                                                  \rightarrow
  h(x+y)$\\
  \hline
{\tiny XOR \& homomorphy} & ACUNh & AC & & & & $x * x * y \rightarrow y $ \\
                 & & $\cup$ U & & & & $ x * 1 \rightarrow x $ \\
                 & & $\cup$ N & & & & $ x * x \rightarrow 1 $ \\
                 & & $\cup$ $\{ h(x+y) = h(x) + h(y) \}$ & & & & \\
  \hline
\end{tabular}

\todo[inline]{In the table, is it better to express regularity/orientability/unifiability globally (for the whole set) or locally (for each equation) when there are several equations involved?}

\paragraph{XOR} We may try two models. We use additive notation, i.e. '$+$' symbol and neutral element $0$.

\begin{example} ACUN, with more unification
  
  $\En = \textrm{AC}, \Ea = \textrm{ACUN}, E= \textrm{ACUN}$ (maybe larger?),

  $\Rn = \{ x + x + y \rightarrow y \} \cup \{ x + 1 \rightarrow x \} \cup \{ x + x \rightarrow 0 \}, \Rm =
  \emptyset$ \\
  Check if Def~\ref{def_ext_sig} is ok :
  \begin{itemize}
  \item $\En \subseteq \Ea \subseteq E$ and $\En$ is regular
    $\rightarrow$ ok
  \end{itemize}
  \end{example}

  \begin{example} ACUN, with more Horn clauses
    
    $\En = \emptyset, \Ea = \textrm{AC}, E= \textrm{ACUN}$ (maybe larger?) ,

    $\Rn = \Rm = \{ x + x + y \rightarrow y \} \cup \{ x + 1 \rightarrow x \} \cup \{ x + x \rightarrow 0 \}$ \\
  Check if Def~\ref{def_ext_sig} is ok :
  \begin{itemize}
  \item $\En \subseteq \Ea \subseteq E$ and $\En$ is regular
    $\rightarrow$ ok
  \end{itemize}
\end{example}

\paragraph{XOR with homomorphy: ACUNh}

\todo[inline]{homomorphy of XOR operator, or of another operator? I
  would say that '$+$' operator designs XOR, so that $h$ is
  homomorphic over XOR.}
  
Remark: to address ACUNh, one cannot address ACUN and h separately
(not disjoint, so no composition result). Both ACUN and ACUNh are of
interest, but then should be addressed separately.

Warning concerning homomorphy: same operator over ``plaintexts/inputs''
and ``ciphertexts/outputs'' (so does not tackle any ``homomorphic''
scenario, in particular in case of homomorphic encryption)

Several flavours for ACUNh\ldots

\begin{example} ACUNh
  
  $\En = \textrm{AC}, \Ea = \textrm{ACUN}, E= \textrm{ACUNh}$ 
  
\end{example}

\begin{example} ACUNh
  
  $\En = \textrm{AC}, \Ea = \textrm{ACUNh}, E= \textrm{ACUNh}$
  
\end{example}
    
\begin{example} ACUNh
      
    $\En = \emptyset, \Ea = \textrm{AC}, E= \textrm{ACUNh}$

\end{example}

% \todo[inline]{check if more variations could be of interest,
%   e.g. introducing homomorphy at different stages, e.g. $AC \subset
%   ACh$ or $ACUN \subset ACUNh$ \ldots}

\todo[inline]{see if we want to try more flavours}

\todo[inline]{check if rewrite rule $h(x) + h(y) \rightarrow h(x+y)$
  is sufficient, or if we need more rules, e.g. $h(0) \rightarrow 0$
  and $-h(x) \rightarrow h(-x)$.}

\todo[inline]{check if definitions match.}

\todo[inline]{check papers \cite{DBLP:conf/rta/LafourcadeLT05,DBLP:conf/icalp/DelauneLLT06,DBLP:journals/jcs/CortierDL06,DBLP:journals/entcs/Lafourcade07}}

Quotation from the LSV Research Report \texttt{rr-lsv-2005-20.pdf} associated with \cite{DBLP:conf/icalp/DelauneLLT06}:
\begin{quotation}
We represent the ACUNh equational theory by an AC-convergent rewrite
system. This can be obtained by orienting from left to right the
equations (U), (N), (h) and by adding the consequence $h(0)
\rightarrow 0$ (see \cite{DBLP:conf/rta/LafourcadeLT05} for
details). After each deduction step, the term $u$ obtained is reduced
to its normal form $u \norm{}{}$. Equivalence modulo AC is easy to
decide, so we omit the equality rule for AC and just work with
equivalence classes modulo AC. Moreover in this paper, we consider
implicitely that terms are kept in normal forms.

[\ldots]

In our model, we can not model the fact that the homomorphism is over
a multiplication, \emph{i.e.} an Abelian group operator. It is for
this that we choose to model it by an homomorphism over $\oplus$. This
operator satisfies more algebraic properties, hence this abstraction
could find some non realistic attacks.

[\ldots]

For instance, unification in Abelian groups with homomorphism has been
shown to be finitary bu Baader [F. Baader. Unification in commutative theories, Hilbert’s basis theorem and Gröbner bases. Journal of the ACM, 40(3):477–503, 1993.] with the help of
Gröbner bases. Therefore, our approach should extend to that case,
thus generalyzing the result [J. Millen and V. Shmatikov. Symbolic protocol analysis with an abelian group operator or Diffie-Hellman exponentiation. Journal of Computer Security, 13(3):515 – 564, 2005.]. Apart from the extension to
Abelian groups, a future line of research is to study the complexity
of the procedure. Contrary to the results obtained in the empty theroy
[M. Rusinowitch and M. Turuani. Protocol insecurity with a finite number of sessions, composed keys is NP-complete. Theoretical Computer Science, 1-3(299):451–475, 2003.], there is little hope to get an NP-procedure: the
similarity between ACUNh unification and AC unification leads to
conjecture that there are exponentially many minimal unifiers and that
the size of a minmal unifier may be exponential.
\end{quotation}

Quotation from \cite{DBLP:conf/rta/LafourcadeLT05}:
\begin{quotation}
  % [in Section 2 ``A Dolev-Yao model for rewriting modulo AC'']
  In fact, this deductive system is equivalent in deductive power to a
  variant of the system in which terms are not automatically
  nomalized, but in which arbitrary equational proofs are allowed at
  any moment of the deduction. The equivalence of the two proof
  systems has been shown in [Comon-Lundh, Treinen, Easy inruder
  detections, LNCS 2772, pp 225-242, 2003] without AC axioms; in []
  this has been extended to the case of a rewrite system modulo AC.

  In the rest of the paper, we will investigate the Dolev-Yao
  deduction system modulo the rewrite systems presented in Figure 2. 
\end{quotation}
where in Figure 2 of the paper we find three rewrite systems modulo
AC: ACh modulo AC, ACUNh modulo AC, and
AGh modulo AC.

\paragraph{DH}

Concerning the definition of the related equational theory:
should we rewrite AG rules like this, or for exponents?
should we add multiplication of exponents?
$$
exp(x,a) \times exp(x,b) = exp(x,a+b)
$$

See Liu's PhD dissertation, page 89:
We give a convergent rewriting system RAGh for UIH modulo AC:
$$
\begin{array}{rcl}
  x + 0  &\rightarrow& x \\
  x + (-x)  &\rightarrow& 0 \\
  -0  &\rightarrow& 0 \\
  -(-x)  &\rightarrow& x \\
  -(x+y)  &\rightarrow& -x + (-y) \\
  h(x) + h(y)  &\rightarrow& h(x+y) \\
  h(0)  &\rightarrow& 0 \\
  -h(x)  &\rightarrow& h(-x) \\
  x + y + (-x)  &\rightarrow& y \\
  h(x) + y + h(z)  &\rightarrow& h(x+z) + y
\end{array}
$$

\todo[inline]{check gap between Liu's PhD dissertation
  and~\cite{smcb-csf12}}

Also see Tamarin's paper~\cite{smcb-csf12}, where DH equational theory also includes:
$$
(x^{-1})^{-1} = x
$$
Should we add it?

\todo[inline]{not in equational theory, but yes in rewriting rules}

Also in~\cite{smcb-csf12}, we can find Lankford's presentation of AG axioms, as:
$$
\begin{array}{rcl}
  (x^{-1} * y)^{-1} &=& x * y^{-1} \\
  x^{-1} * y^{-1} &=& (x * y)^{-1} \\
  x * (x * y)^{-1} &=& y^{-1} \\
  x^{-1} * (y^{-1} * z) &=& (x * y)^{-1} * z \\
  (x * y)^{-1} * (y * z) &=& x^{-1} * z \\
  1 ^{-1} &=& 1 \\
  x * 1 &=& x \\
  (x^{-1})^{-1} &=& x \\
  x * (x^{-1} * y) &=& y \\
  x * x^{-1} &=& 1
\end{array}
$$
which can be oriented in the following set of rewriting rules:
$$
\begin{array}{rcl}
  (x^{-1} * y)^{-1} &\rightarrow& x * y^{-1} \\
  x^{-1} * y^{-1} &\rightarrow& (x * y)^{-1} \\
  x * (x * y)^{-1} &\rightarrow& y^{-1} \\
  x^{-1} * (y^{-1} * z) &\rightarrow& (x * y)^{-1} * z \\
  (x * y)^{-1} * (y * z) &\rightarrow& x^{-1} * z \\
  1 ^{-1} &\rightarrow& 1 \\
  x * 1 &\rightarrow& x \\
  (x^{-1})^{-1} &\rightarrow& x \\
  x * (x^{-1} * y) &\rightarrow& y \\
  x * x^{-1} &\rightarrow& 1
\end{array}
$$
We should check if all these rules are necessary in our case or not, for DH modulo AC.
%%
%%% Section
%%

\section{Draft}

\subsection{Adding XOR handling in ProVerif}

Let $E$ be an equational theory equal to $E_{CL} \uplus E'$, where $E_{CL}$ is the union of convergent (terminating and confluent) or linear  equational theories, and $E'$ is neither a convergent nor a linear equational theory.

Let $\Sigma$ be a signature equal to $(\emptyset, E)$.

\textit{N.B. : In our problem, $E'$ would be $E_{xor}$.}

\begin{conjecture}
  \label{conj:fvp-equiv}
  If $(E,E')$ has the finite variant property, then there exists a rewriting system $\R'$ such that $(\R', E')$ models $\Sigma$.
\end{conjecture}

\textit{N.B. : read Comon and Delaune's paper~\cite{DBLP:conf/rta/Comon-LundhD05} to read the definition of the \emph{finite variant property} and~\cite[Lemma~3]{DBLP:conf/rta/Comon-LundhD05} (which will be useful to complete the proof of the aforementioned conjecture).}

\paragraph{Hypothesis 0.}

To begin with, we assume that $E'$ is empty. In other words, we want to prove that the finite variant property implies modelisation as defined by Bruno Blanchet in \cite[Definition~4]{BlanchetAbadiFournetJLAP07}.

\paragraph{Hypothesis 1.}

\[\Sigma_{user} \cap \Sigma_{xor} = \emptyset\] 
\begin{align}
  \notag\Sigma &= \Sigma_{user}^{conv} \uplus \Sigma_{user}^{lin} \uplus \Sigma_{xor} \\
  \notag& = (\emptyset, E_{user}^{conv}) \uplus (\emptyset, E_{user}^{lin}) \uplus (\emptyset, E_{xor}) \\
  \notag&\;\downarrow \text{\textbf{\footnotesize Algorithm 3}} \\
  \notag\Sigma' &= ({\R'}_{user}^{conv}, \emptyset) \uplus ({\R'}_{user}^{lin}, \emptyset) \uplus (\emptyset, E_{xor}) \\
  \notag& = (\R'_{user}, E_{xor})
\end{align}

where $\R'_{user} = {\R'}_{user}^{conv} \uplus {\R'}_{user}^{lin}$
%\[\downarrow \text{\textbf{\footnotesize algorithm 3}} \downarrow\]
%\[\Sigma' = (\R'_{user}, \emptyset) \uplus (\emptyset, E_{xor}) = (\R'_{user}, E_{xor})\]

%\begin{minipage}{.8\textwidth}%

\paragraph{Goal 1 (sanity check).}
Verify that algorithm 3 handles $\Sigma_{user} \uplus \Sigma_{xor}$.

\paragraph{Goal 2.}

Let $\Sigma_{CL}$ be a signature equal to $(\emptyset, E_{CL})$ and let $\Sigma'_{CL}$ be a signature modeling $\Sigma_{CL}$ according to~\cite[Definition~4]{BlanchetAbadiFournetJLAP07}, such that $\Sigma'_{CL}$ equals $(\R', \emptyset)$, where $\R'$ is a rewriting system.
Let $\Sigma'$ be a signature equal to $(\R', E')$.

We want to determine whether $\Sigma'$ models $\Sigma$.
We cannot use Blanchet's definition to determine this because the equational theory of $\Sigma'$ is not syntactic equality.
\textbf{We first need to find a more general definition of ``\emph{$\Sigma'$ models $\Sigma$}''.}
Then, we will try to prove Conjecture~\ref{conj:fvp-equiv}.

%Let $\mathtt{def}_{\Sigma}(f)$ be $\{ f(x_1, ..., x_n) \rightarrow f(x_1, ..., x_n) \}$ when $f$ is a constructor of arity $n$.

\begin{definition}
  $\Sigma'$ models $\Sigma$ iff:
  \begin{enumerate}[label*=\arabic*.]
  \item The equational theory of $\Sigma'$ is $E'$: $\Sigma' \vdash M = N$ if and only if $M =_{E'} N$.
  \item The constructors of $\Sigma'$ are the constructors of $\Sigma$; $\R_c$ contains the rule $f(x_1, ..., x_n) \rightarrow f(x_1, ..., x_n)$, plus perhaps other rules such that there exists a rewriting system $\R$ on terms that satisfies the following properties:
    \begin{enumerate}[label*=\arabic*.]
    \item If $M \rightarrow N \in \R$, then $M =_{E} N$.
    \item {\textbf{\color{red} $\diamond$ redefine $\mathtt{nf}_{\R,E'}$ :}
      
      \textbf{[Old Definition]} We say that the set of terms $\Mset$ is in normal form relatively to $\R$ and $\Sigma$, and write $\mathtt{nf}_{\R,\Sigma}(\Mset)$, if and only if all terms of $\Mset$ are irreducible by $\Sigma$ and, for all subterms $N_1$ and $N_2$ of terms of $\Mset$, if $\Sigma \vdash N_1 = N_2$ then $N_1 = N_2$.

      \textbf{[New Definition]} We say that the set of terms $\Mset$ is in normal form relatively to $\R$ and $\Sigma$, and write $\mathtt{nf}_{\R,\Sigma}(\Mset)$, if and only if all terms of $\Mset$ are irreducible by $\Sigma$ and, for all subterms $N_1$ and $N_2$ of terms of $\Mset$, if $\Sigma \vdash N_1 = N_2$ then $N_1 =_{E'} N_2$.

      \textbf{\color{red} $\diamond$ redefine the notion of irreducibility :}

      \textbf{[Old Definition]} We say that a term is irreducible by $\R$ when none of the rewrite rules of $\R$ applies to it.

      \textbf{[New Definition v1]} We say that a term is irreducible by $\Sigma$ when none of the rewrite rules of $\R$ and none of the rewrite rules obtaining from orientable (w.r.t. a given reduction ordering) equations of $E'$ applies to it.
      
      \textbf{[New Definition v2]} We say that a term $t$ is irreducible by $\Sigma$ when $\exists t', t \rightarrow_\R t' \Rightarrow t =_{E'} t'$.\\

      If $\mathtt{nf}_{\R,E}(\Mset)$, then for any term $M$ there exists a term $M'$ such that $M' =_{E} M$ and $\mathtt{nf}_{\R,E}(\Mset \cup \{M'\})$.
    }
    \item If $f(N_1, ..., N_n) \rightarrow N \in \R_c$, then $f(N_1, ..., N_n) =_E N$.
    \item If $f(M_1, ..., M_n) =_E M$ and $\mathtt{nf}_{\R,E,E'}(\{M_1, ..., M_n, M\})$, then there exist $\sigma$ and\\        
      {\color{blue}(1)}$\;\bullet\; f(N_1, ..., N_n) \rightarrow N \in \R_c$ such that $M =_{E'} N\sigma$ and $M_i =_{E'} N_i\sigma$ for all $i \in \{1, ..., n\}$.\\
      {\color{blue}(2)}$\;\bullet\; N \rightarrow N' \in \R_c$ such that $N\sigma =_{E'} f(M_1, ..., M_n)$ and $M =_{E'} N'\sigma$.\\

      {\color{blue}(1)} $\Rightarrow$ {\color{blue}(2)} but {\color{blue}(2)} $\nRightarrow$ {\color{blue}(1)}\\
      {\color{blue}(1)} is preferred, but {\color{blue}(2)} may be necessary
    \end{enumerate}
  \end{enumerate}

  \paragraph{Hypothesis 2.}
  $\Sigma_{user}$ may contain the XOR function symbol, but not at top-level.

  \paragraph{Hypothesis 3.}
  $\Sigma_{user}$ may contain the XOR function symbol, including at top-level.

  \subsubsection{Plan}

  \begin{enumerate}[label*=\arabic*.]
  \item Prove Conjecture~\ref{conj:fvp-equiv} with Hypothesis 0.\\
    \textcolor{blue}{\texttt{[brainstorming]}\\
      \textbf{Conjecture 1B}: For any convergent or linear equational theory $E$, there exists $\Sigma^\prime$ that models \cite[Def. 4]{BlanchetAbadiFournetJLAP07} $(\emptyset, E)$.\\
      \underline{Question 1B}: Is it necessary to prove Conjecture 1B?\\
      \underline{Question 2B}: Does $(E_{CL}, \emptyset)$ has the finite variant property?\\
      \textbf{Conjecture 2B}: If ground terms set is finite, then $(E_{CL}, \emptyset)$ has the finite variant property.\\
    (draft) Bruno says that his algorithms ``do not always terminate because, for some equational theories, they generate an unbounded number of rewrite rules''. We need to figure out when exactly they do not terminate in order to determine if it is just a complexity problem or a theoretical one. In the first case, that will help us to prove Conjecture 1B.}
  \item Goal 1.
  \item Prove Conjecture~\ref{conj:fvp-equiv} with Hypothesis 1.
  \item Consider Hypothesis 2. Let $\R_1$ be a rewriting system and $E_1$ an equational theory. Suppose $(\R_1, E_1)$ has the finite variant property. Let $\Sigma$ be the signature equal to $(\R_1, E_1)$.
    \begin{enumerate}[label*=\arabic*.]
    \item Suppose $E_1$ is \textbf{regular} (cf.~\cite{DBLP:conf/rta/Comon-LundhD05}) and $\R_1$ is \textbf{convergent}.\\
      {\color{red}\underline{Question} : $\exists?\,\R_2, E_2,\;\;(\R_2, E_2)$ models $\Sigma$}\\
      (where $E_2$ contains $E_1$ + some rules from $\R_1$.)\\
      \textbf{update:} $E_1 = E_2$
    \item {\color{red}\underline{Biblio}} : What does work? Is it necessary for $E_1$ to be regular?
    \item {[depending on the answer of the previous question]}
      Suppose $E_1$ is \textbf{non-regular} (or at least ``partially'') and $\R_1$ is \textbf{convergent modulo $\mathbf{E_1}$}.\\
      {\color{red}\underline{Question} : $\exists?\,\R_2,\;\;(\R_2, E_1)$ models $\Sigma$}\\
      \emph{N.B. : This case is more interesting for us.}
    \item Suppose $E_1$ convergent modulo $E_2$, and $\Sigma = (\emptyset, E_1 \cup E_2)$.
      \begin{enumerate}[label*=\arabic*.]
      \item {\color{red}\underline{Question} : $\exists?$ algo able to find $\R_1,\;\;(\R_1, E_2)$ models $\Sigma$}
      \item {\textbf{update:} Suppose we find $\Sigma'$ that models $\Sigma$ such that $\Sigma' = (\R_1, E_2)$.}\\
      {\color{red} \underline{Question} : To what extent can we add rules to $\R_1$ from equations from $E_2$, such that the resulting $\Sigma''$ keeps modeling $\Sigma$?}\\
      {The resulting $\Sigma''$ would be only used to simplify equations (normalization), but we will keep working with $\Sigma'$ after that simplification.}
      \end{enumerate}
    \item \verb![!ambitious\verb!]! Idem but $E_2$ not necessarily linear.
    \end{enumerate}
  \item {\color{blue}[bonus]} Blanchet's $2^{nd}$ algorithm : input=$(\emptyset, E)$.
    \begin{conjecture}
      If Algorithm 2 terminates when $E$ is non-linear, then there exists $\Sigma'$ such that $\Sigma'$ models $\Sigma$.
    \end{conjecture}
    Prove it.
    
    \emph{Kind reminder : Algorithm 2 is correct when $E$ is linear.}
    \item {\color{red}\underline{Biblio}} : % When $E$ is a linear
      % equational theory, how to determine that a rewriting system $\R$
      % is convergent modulo $E$? (e.g. Knuth-Bendix modulo $E$?)
      Let $E = E_1 \cup E_2$. We have several questions we would like to answer to help us improve Proverif tool. For each question, we would like to know if the answering algorithm:
\begin{itemize}
  \item may not terminate,
  \item may ``fail'',
  \item runs well in practice.
\end{itemize}

\begin{description}

\item[Q1:] \texttt{For Proverif to take care automatically} \\
  ``$\exists ? \mathcal{R}$ such that $\mathcal{R}$ is $E_2$-convergent for $E$?'' \\
  Considering the respective assumpions (to be considered sequentially):
  \begin{description}
  \item[H1:] unification modulo $E_2$ is decidable,
  \item[H2:] $E_2$ is regular,
  \item[H3:] $E_2$ is linear,
  \item[H4:] $E_2$ is regular, linear, and includes $E_{xor}$,
  \item[H5:] $E_2=E_{xor}$
  \end{description}

  Answers: H3 no, H4 no, H5 no (see mails from F. Blanqui and J.P. Jouannaud)

\item[Q2:] \texttt{User will give R as input, and Proverif will only have to check} \\
  ``Given $\mathcal{R}$ and $E_2$, is $\mathcal{R}$ convergent modulo $E_2$?''\\
  Assuming \textbf{H1}? \textbf{H2}? \textbf{H3}? \textbf{H4}? \textbf{H5}? 

Answer: probably linked to Th 19 from J.P. Jouannaud's book. To be checked.
  
\item[Q3:] 
  ``Let $E_1$ and $E_2$ be such that $E_1 \cap E_2 = \emptyset$ and unification is decidable for each of them separately.
  Can we get an algorithm to unify $E_1 \cup E_2$?''

\item[Q4:] ``Does Ocaml code exist to manage the questions above?''
\end{description}

\url{https://blanqui.gitlabpages.inria.fr/useful.html}

Termination checkers:
\begin{itemize}
\item AProVE \url{http://aprove.informatik.rwth-aachen.de/}
\item TTT2 \url{http://cl-informatik.uibk.ac.at/software/ttt2/}
\newcommand\TTTT{%
 \textsf{T\kern-0.15em\raisebox{-0.55ex}T\kern-0.15emT\kern-0.15em\raisebox{-0.55ex}2}%
}
\item CiME3 \url{http://cime.lri.fr/}
\item MatchBox \url{https://github.com/jwaldmann/matchbox}
\item TORPA \url{http://www.win.tue.nl/~hzantema/torpa.html}
\item Jambox \url{http://joerg.endrullis.de/}
\item MU-Term \url{http://zenon.dsic.upv.es/muterm/}
\end{itemize}

Confluence checkers:
\begin{itemize}
\item ACP \url{http://www.nue.riec.tohoku.ac.jp/tools/acp/}
\end{itemize}

% (VAR x y z)
% (RULES
%     plus(0,y) -> y
%     plus(y,y) -> 0
%     plus(x,y) -> plus(y,x)
%     plus(x,plus(y,z)) -> plus(plus(x,y),z)
%     )

  \end{enumerate}  

  \subsubsection{New plan (2022-03-15)}

  \begin{enumerate}
  \item[1] Th 19 from J.P. Jouannaud's book: Knuth-Bendix with modulo,
    to build an algorithm
    \begin{itemize}
    \item $\mathcal{R} + \mathcal{R}_{XOR}, E=E_{XOR}$
    \item $\mathcal{R} + \mathcal{R}_{XOR}, E=E_{AC}$
      \end{itemize}
  \item[1.5] to derive an algorithm for finite variants modulo E?
  \item[2] ``linear'' algorithm (Algo 2 from Bruno's paper), taking into account $\mathcal{R}$
  \item[2.5] general algorithm, taking into account $\mathcal{R}$ [show
    that the algorithm, if terminating, works for any equational theory]
  \item[3] unification of disjoint theories
  \end{enumerate}
  
\end{definition}



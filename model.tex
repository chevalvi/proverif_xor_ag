%%
%%% Section
%%

\section{Model}

%%
%%% Subsection
%%

\subsection{Terms, Substitutions, Unification}

We use classical notations and terminology from \cite{DBLP:conf/rta/Comon-LundhD05} on terms, unification, rewrite systems.
$\T(\F,\X)$ is the set of terms built over the finite (ranked) alphabet $\F$ of function symbols and the set of variable symbols $\X$ . $\T(\F,\emptyset)$ is also written $\T(\F)$.
The set of positions of a term $t$ is written $\Pos{t}$.
The subterm of $t \in \T(\F,\X)$ at position $p \in \Pos{t}$ is written $t|_p$.
The term obtained by replacing $t|_p$ with $u$ is denoted $t[u]_p$ . The set of variables occurring in $t$ is denoted $vars(t)$.
A substitution $\sigma$ is a mapping from a finite subset of $\X$ called its domain and written $\dom{\sigma}$ to $\T(\F,\X)$.
If $E$ is a set of equations (unordered pair of terms), $=_E$ is the least congruence on $\T(\F,\X)$ such that $u\sigma =_E v\sigma$ for all pairs $u = v \in E$ and substitutions $\sigma$.
$E$ is \textit{regular} if, for every equation $t_1 = t_2 \in E$, $vars(t_1) = vars(t_2)$. Two terms $s, t$ are $E$-unifiable if there is a substitution $\sigma$ such that $s\sigma =_E t\sigma$.
Such a substitution is called an $E$-unifier of $s, t$.

We way that a finite set of substitutions $S = \{\sigma_1,\ldots,\sigma_n\}$ is a most general set of $E$-unifiers of $s,t$, if for all $E$-unifier $\sigma$ of $s,t$, there exists $i \in \{1,\ldots,n\}$ and a substitution $\theta$ such that $\sigma =_E \sigma_i\theta$. We denote by $\mgu{E}{s,t}$ the set of most general sets of $E$-unifiers of $s,t$. Note that $\mgu{E}{s,t}= \emptyset$ implies that $s,t$ are not unifiable. Also, note that when $E = \emptyset$, if $s$ and $t$ are unifiable then $\mgu{E}{s,t}$ always contains a singleton corresponding to the classical syntactic most general unifier.

We say that an algorithm $\Alg$ is a partial $E$-most general unifier algorithm if for any two terms $s,t$, $\Alg(s,t)$ outputs either $\bot$ or $S \in \mgu{E}{s,t}$.
Intuitively, $\Alg(s,t) = \bot$ indicates that the algorithm $\Alg$ is not able to compute the set of most general unifiers. As expected, we say that $\Alg$ is an $E$-most general unifier algorithm if it is a partial $E$-most general unifier algorithm that nevers outputs $\bot$. 

In the rest of this paper, when write $\mguAlg{E}{s,t} = S$, we implicitely assume that we have a partial $E$-most general unifier algorithm $\Alg$ such that $\Alg(s,t) = S$.
This specification will allow us to consider equational theory for which there is on partial $E$-most general unifier algorithm such as for associative equational theory.

%%
%%% Subsection
%%

\subsection{Equational Rewriting}

A term rewrite system (TRS) is a finite set of \textit{rewrite rules} $\ell \rightarrow r$ where $\ell \in \T(\F,\X)$ and $r \in \T(\F,vars(\ell))$.
A term $s \in \T(\F,\X)$ rewrites to $t$ by a TRS $\R$, denoted $s \rightarrow_R t$, if there is $\ell \rightarrow r$ in $\R$, $p \in \Pos{s}$ and a substitution $\sigma$ such that $s|_p = \ell\sigma$ and $t = s[r\sigma]_p$.

Given a rewrite system $\R$ and an equational theory $E$, \emph{$s$ rewrites into $t$ by $\R$ modulo $E$}, denoted $\rwstep{\R}{E}$, iff there exists a position $p \in \Pos{s}$, a rule $\ell \rightarrow r \in \R$ and a substitution $\sigma$ such that $s|_p =_E \ell\sigma$ and $t = s[r\sigma]_p$.

A rewrite system $\R$ is $E$-confluent if and only if for all $s,t$, if $s =_{\R^= \cup E} t$ then there exist $s',t'$ such that $s \rwsteps{\R}{E} s'$, $t \rwsteps{\R}{E} t'$ and $s' =_E t'$. $\R$ is said to be $E$-convergent if, in addition, it is terminating.
A term $t$ is in normal form (w.r.t. $\rwstep{\R}{E}$) if there is no term $s$ such that $t \rwstep{\R}{E} s$. When $\R$ is $E$-convergent, we write $s = t\norm{\R}{E}$ the unique normal form of $t$.

%%
%%% Subsection
%%

\subsection{ProVerif syntax and semantics on terms}

In ProVerif, the set of function symbols is partitioned into $\F = \Fc \uplus \Fd$ where $\Fc$ (resp. $\Fd$) contains the set of constructor (resp. destructor) function symbols. Intuitively, constructor function symbols build data whereas destructor function symbols can manipulate data and only appear in the evalation of terms. A \emph{constructor term} is a term in $\T(\Fc,\X)$.

The behavior of a destructor function symbol $g$ is modeled by an ordered list of rewrite rules of the form $g(U_1,\ldots, U_n) \rightarrow U$ where $U_1,\ldots, U_n, U$ are \emph{may-fail terms}, that are either terms $M$ or the constant $\fail$ or a \emph{may-fail variable} $u$. A may-fail substitution $\sigma$ is substitution on may-fail terms satisfying the following: for all variables $x \in \dom{\sigma}$, $x\sigma$ can only be a term, i.e. not $\fail$ nor a may-fail variable. Unification is extended to may-fail terms as expected: $U$ and $V$ are unifiable modulo $E$ if there exists a may-fail substitution $\sigma$ such that $U\sigma =_E V\sigma$. Note that by definition, $\fail$ is not unifiable with a term or even a variable $x$. It can only be unifiable with $\fail$ itself or a may-fail variable.

A signature $\Sigma$ is defined as a pair $(\defrw{\Sigma},E)$ where $\defrw{\Sigma}$ is a function associating destructor function symtbols to ordered lists of rewrite rules; and $E$ is an equational theory.

\begin{figure}[ht]
  \begin{center}
  \begin{minipage}{\columnwidth}
    \begin{tabbing}
      $M,$ \= $N$ ::= \hspace{5.5cm} \=    ter\=ms\\
      \> $x$ \>\> variable ($x\in \X$)\\
      \> $n$\>\> name ($n \in \N$)\\
      \> $f(M_1,\ldots, M_k)$\>\> applied $f \in \Fc$\\
      \\
      $D$ ::=\>\>expressions\\
      \> $M$ \>\> term\\
      \> $h(D_1,\ldots,D_k)$\>\> applied $h\in \Fd \cup \Fc$\\
      \> $\fail$\>\> failure
    \end{tabbing}
    \end{minipage}
  \end{center}
  \caption{Syntax of the term language of ProVerif.}
  \label{fig:syntax}
  \end{figure}

Let $g \in \Fd$. Let $V_1,\ldots, V_n$ be may-fail terms. We say that $g(V_1,\ldots, V_n)$ can be rewritten by $g(U_1,\ldots, U_n) \rightarrow U$ in  $\defrw{\Sigma}(g)$ if there exists $\sigma$ such that for all $i \in \{1,\ldots,n\}$, $U_i\sigma =_E  V_i$.

The evaluation of an expression $D$, denoted $D \Eval{\Sigma}$, is as follows:
\begin{itemize}
\item $M \Eval{\Sigma} M$
\item $\fail \Eval{\Sigma} \fail$
\item $f(D_1,\ldots,D_n) \Eval{\Sigma} V$ if $f/n\in \Fc$ and either $D_i \Eval{\Sigma} V_i$, $V_i \neq \fail$ for all $i = 1 \ldots n$ and $V = f(V_1,\ldots,V_n)$, or else $V = \fail$
\item $g(D_1,\ldots,D_n) \Eval{\Sigma} V$ if $g/n \in \Fd$ and $D_i \Eval{\Sigma} V_i$ for all $i = 1\ldots,n$ and 
\begin{itemize}
  \item either $V = \fail$ and $g(V_1,\ldots,V_n)$ cannot be rewritten by any rewrite rules in $\defrw{\Sigma}(g)$
  \item otherwise $V = U\sigma$ with $g(U_1,\ldots,U_n) \rightarrow U$ the first rule in $\defrw{\Sigma}(g)$ that can rewrite $g(V_1,\ldots,V_n)$ and for all $i \in \{1,\ldots,n\}$, $U_i\sigma =_E  V_i$.
\end{itemize}
\end{itemize}

%%
%%% Paragraph
%%

\paragraph{Link between rewriting modulo $E$ and term evaluation}

The evaluation of expression does not fit exactly standard rewriting definition, first due to the semantics of destructors given by a sequence of rewrite rules rather than a set of rewrite rules, and second due to the presence of may-fail terms in the rewrite rules. However, if we restrict ourselves to destructors defined by a single rewrite rules containing only terms then the evaluation $\Eval{\Sigma}$ corresponds in fact to a bottom-up evaluation of $\rwstep{\R}{E}$ where $R = \bigcup_{g \in \Fd} \defrw{\Sigma}(g)$. In particular, $D \Eval{\Sigma} M$ implies $D \rwsteps{\R}{E} M$.
